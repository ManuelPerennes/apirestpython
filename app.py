from flask import Flask, escape, request
from flask_pymongo import PyMongo
from flask import render_template
from flask import url_for

api = Flask(__name__)

@api.route('/')
def accueil():
    return render_template('accueil.html')

@api.route('/api/v1/topics/')
def topics():
    return render_template('topics.html')

@api.route('/api/v1/topicsId')
def topicsId():
    return render_template('topicsId.html')

api.run(host='0.0.0.0', port=1236)

